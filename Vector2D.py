import math

class Vector2D:
    def __init__(self,x=0.0,y=0.0):
        self.x = x
        self.y = y

    def __add__(self,vector):
        x = self.x + vector.x
        y = self.y + vector.y
        return Vector2D(x,y)

    def __neg__(self):
        return Vector2D(-self.x,-self.y)

    def __sub__(self,vector):
        return (self + (-vector))

    def __mul__(self,factor):
        x = self.x * factor
        y = self.y * factor
        return Vector2D(x,y)

    def __truediv__(self,divisor):
        x = self.x / divisor
        y = self.y / divisor
        return Vector2D(x,y)

    def __repr__(self):
        return "v("+str(self.x)+","+str(self.y)+")"

    def length(self):
        return math.sqrt(self.x * self.x + self.y * self.y)

    def normalize(self):
        tmp = self / self.length()
        self.x = tmp.x
        self.y = tmp.y

    def dot(self, v):
        return self.x*v.x + self.y*v.y

    def sign(self,v):
        if(self.y*v.x > self.x*v.y ):
            return 1
        else:
            return -1
