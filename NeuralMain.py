#!/bin/env python3

import Utils
import time
from tkinter import *
import tkinter.messagebox as messagebox

from Controler import Controller
from Timer import Timer
from Params import Params

#///////////////////////GLOBALS ////////////////////////////////////

szApplicationName = "Smart Sweepers v1.0"
szWindowClassName = "sweeper"


#The controller class for this simulation
controller	 = None

def reset(hwnd):
	if controller != None:
		controller = None

	controller = Controller(hwnd)
	
def draw(controller):
	# draw the board according to the current state
	controller.update()
	controller.render()
	self.after(16, lambda: draw(controller))

#-----------------------------------WinMain-----------------------------------------
#	Entry point for our windows application
#-----------------------------------------------------------------------------------
def WinMain():

	winclass = Tk()

	# create the window (one that cannot be resized)
	hwnd = Canvas(winclass, width=Params.WindowWidth, height=Params.WindowHeight)
	controller = Controller(hwnd)

	hwnd.bind('<Escape>', lambda : winclass.destroy())
	hwnd.bind('F', lambda : controller.fastRenderToggle())
	hwnd.bind('R', lambda : reset(hwnd))
	hwnd.pack()
	hwnd.after(16, lambda :draw(controller))

	#create a timer
	timer = Timer(Params.iFramesPerSecond)

	#start the timer
	timer.start()

	#Show the window
	winclass.mainloop()

if __name__ == "__main__" :
	WinMain()
