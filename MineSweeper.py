import NeuralNet
import Utils
import SimpleMatrix
import Vector2D
import Params
import math

from Params import Params
from random import random
from Vector2D import Vector2D
from NeuralNet import CNeuralNet
from SimpleMatrix import Simple3x3Matrix

class MineSweeper:
    def __init__(self):
        self.itsBrain = CNeuralNet()
        self.vPosition = Vector2D(random() * Params.WindowWidth, random()*Params.WindowHeight)
        self.vLookAt = Vector2D()
        self.rotation = random()*Params.dTwoPi
        self.speed = 0
        self.lTrack = 0.16
        self.rTrack = 0.16
        self.dFitness = 0
        self.scale = Params.iSweeperScale
        self.closestMine = 0

    def incrementFitness(self):
        self.dFitness += 1

    def putWeights(self, vWeights):
        self.itsBrain.putWeights(vWeights)

    def getNumberOfWeights(self):
        return self.itsBrain.getNumberOfWeights()

    def reset(self):
        self.vPosition = Vector2D( random()*Params.windowsWidth, random()*Params.windowHeight)
        self.dFitness = 0
        self.rotation = rondom()*Params.dTwoPi

    def worldTranform(self, vSweeper):
        matTransform = Simple3x3Matrix()
        matTransform.scale(self.scale, self.scale)
        matTransform.rotate(self.rotation)
        matTransform.translate(self.vPosition.x, self.vPosition.y)

        matTransform.transformSPoints(vSweeper)

    def update(self, vMines):
        vInputs = []

        vClosestMine = self.getClosestMine(vMines)
        vClosestMine.normalize()

        vInputs.append(vClosestMine.x)
        vInputs.append(vClosestMine.y)
        vInputs.append(self.vLookAt.x)
        vInputs.append(self.vLookAt.y)

        output = self.itsBrain.update(vInputs)

        if(len(output) < Params.iNumOutputs):
            return False

        self.lTrack = output[0]
        self.rTrack = output[1]

        rotForce = self.lTrack - self.rTrack
        rotForce = Utils.Clamp(rotForce, Params.dMaxTurnRate, Params.dMaxTurnRate)

        self.rotation += rotForce

        self.speed = self.lTrack + self.rTrack

        self.vLookAt.x = -math.sin(self.rotation)
        self.vLookAt.y = math.cos(self.rotation)

        self.vPosition += (self.vLookAt * self.speed)

        if(self.vPosition.x > Params.WindowWidth):
            self.vPosition.x = 0
        if(self.vPosition.x < 0):
            self.vPosition.x = Params.WindowWidth
        if(self.vPosition.y > Params.WindowHeight):
            self.vPosition.y = 0
        if(self.vPosition.y < 0):
            self.vPosition.y = Params.WindowHeight

        return True

    def getClosestMine(self, vMines):
        closestSoFar = 99999999
        vClosestObject = Vector2D(0,0)

        for i in range(len(vMines)):
            lenToObject = (vMines[i] - self.vPosition).length()

            if (lenToObject < closestSoFar):
                closestSoFar = lenToObject
                vClosestObject = self.vPosition - vMines[i]
                self.closestMine = i

        return vClosestObject

    def checkForMine(self, vMines, size):
        distToObject = self.vPosition - vMines[self.closestMine]

        if(distToObject.length() < (size + 5)):
            return self.closestMine

        return -1


