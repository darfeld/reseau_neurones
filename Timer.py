import time

class Timer:
    def __init__(self, currentTime = 0, lastTime = 0, nextTime = 0, frameTime = 0, \
            timeElapsed = 0.0, fps = 0.0):
        self.currentTime = currentTime
        self.lastTime = lastTime
        self.frameTime = frameTime
        self.timeElapsed = timeElapsed
        self.fps = fps

    def FPS(self,fps):
        self.fps = fps
        self.timeElapsed = 0.0
        self.lastTime = 0
        self.perfCountFreq = 0

        self.frameTime = 1/self.fps


    def start(self):
        self.lastTime = time.perf_counter()
        self.nextTime = self.lastTime + self.frameTime

    def readyForNextFrame(self):
        if(self.fps == 0):
            print("No FPS set in timer")
            return False
        self.currentTime = time.perf_counter()

        if(self.currentTime > self.nextTime):
            self.timeElapsed = self.currentTime - self.lastTime
            self.lastTime = self.currentTime

            self.nextTime = self.currentTime + self.frameTime
            return True

        return False

    def getTimeElapsed(self):
        self.currentTime = time.perf_counter()
        self.timeElapsed = self.currentTime - self.lastTime
        self.lastTime = self.currentTime
        return self.timeElapsed
