class Point:
    def __init__(self, x = 0, y = 0):
        self.x = x
        self.y = y

def Clamp(value, _min, _max):
    if(value < _min):
        value = _min

    if(value > _max):
        value = _max

    return value

