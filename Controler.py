from MineSweeper import MineSweeper
from GenAlg import GenAlg
from GenAlg import Genome
from SimpleMatrix import Simple3x3Matrix
from Vector2D import Vector2D
from random import random

from tkinter import *
import tkinter.messagebox as messagebox

import Utils
from Params import Params

sweeper = [ [-1,-1],
            [-1,1],
            [-0.5, 1],
            [-0.5,-1],
            [0.5,-1],
            [1,-1],
            [1,1],
            [0.5,1],
            [-0.5,-0.5],
            [0.5,-0.5],
            [-0.5,0.5],
            [-0.25,0.5],
            [-0.25,1.75],
            [0.25,1.75],
            [0,25,0.5],
            [0.5,0.5]]

numSweeperVerts = len(sweeper)

mine = [[-1,-1],
        [-1,1],
        [1,1],
        [1,-1]]

numMineVerts = len(mine)

class Controller:
	def __init__(self, hwndMain):
		self.numSweepers = Params.iNumSweepers
		self.numMines = Params.iNumMines
		self.hwndMain = hwndMain

		self.fastRender = False
		self.ticks = 0
		self.generations = 0
		self.cxClient = Params.WindowWidth
		self.cyClient = Params.WindowHeight
		self.vecBestFitness = []
		self.vecAvFitness = []
		self.vSweepers = []
		self.vMines = []
		self.vSweeperVB = []
		self.vMineVB = []

		for i in range(Params.iNumSweepers):
			self.vSweepers.append(MineSweeper())

		self.numWeightsInNN = self.vSweepers[0].getNumberOfWeights()

		self.genAlg = GenAlg(self.numSweepers, Params.dMutationRate, Params.dCrossoverRate, self.numWeightsInNN)

		self.vPopulation = self.genAlg.getChromos()
		print("init pop size = " + str(len(self.vPopulation)))

		for i in range(Params.iNumSweepers):
			self.vSweepers[i].putWeights(self.vPopulation[i].vecWeights)

		for i in range(Params.iNumMines):
			self.vMines.append(Vector2D(random() * self.cxClient, random()*self.cyClient))

		#self.redPen = self.createPen(SOLID, 1, RGB(255,0,0))
		#self.bluePen = self.createPen(SOLID, 1, RGB(0,0,255))
		#self.greenPen = self.createPen(SOLID, 1, RGB(0,255,0))
		self.oldPen = "red"

		for s in sweeper:
			self.vSweeperVB.append(s)

		for m in mine:
			self.vMineVB.append(m)

		self.vAvFitness = []
		self.vBestFitness = []





	def fastRenderToggle(self):
		self.fastRender = not self.fastRender

	def WorldTransform(VBuffer, vPos):
		#create the world transformation matrix
		matTransform = Simple3x3Matrix()

		#scale
		matTransform.scale(Params.dMineScale, Params.dMineScale)

		#translate
		matTransform.translate(vPos.x, vPos.y)

		#transform the ships vertices
		matTransform.transformSPoints(VBuffer)

	def update(self):

		#run the sweepers through CParams::iNumTicks amount of cycles. During
		#this loop each sweepers NN is constantly updated with the appropriate
		#information from its surroundings. The output from the NN is obtained
		#and the sweeper is moved. If it encounters a mine its fitness is
		#updated appropriately,
		if self.ticks < Params.iNumTicks:
			self.ticks += 1
			for i in range(self.numSweepers):
				#update the NN and position
				if not self.vSweepers[i].update(self.vMines):
					#error in processing the neural net
					#MessageBox(m_hwndMain, "Wrong amount of NN inputs!", "Error", MB_OK);
					messagebox.showerror(self.hwndMain, "Wrong amount of NN inputs!")
					print("Error: Wrong amount of NN inputs!")
					return False

				#see if it's found a mine
				GrabHit = self.vSweepers[i].checkForMine(self.vMines, Params.dMineScale)

				if (GrabHit >= 0):
					#we have discovered a mine so increase fitness
					self.vSweepers[i].incrementFitness();

					#mine found so replace the mine with another at a random
					#position
					self.vMines[GrabHit] = Vector2D(random() * cxClient, random() * cyClient);

				print("population len : " + str(len(self.vPopulation)))
				print("sweepers len : " + str(len(self.vSweepers)))
				#update the chromos fitness score
				self.vPopulation[i].dFitness = self.vSweepers[i].dFitness;


		#Another generation has been completed.

		#Time to run the GA and update the sweepers with their new NNs
		else:
			#update the stats to be used in our stat window
			self.vecAvFitness.append(self.genAlg.averageFitness());
			self.vecBestFitness.append(self.genAlg.bestFitness());

			#increment the generation counter
			self.generations += 1;

			#reset cycles
			self.ticks = 0;

			#run the GA to create a new population
			self.vPopulation = self.genAlg.epoch(self.vPopulation);

			#insert the new (hopefully)improved brains back into the sweepers
			#and reset their positions etc
			for i in range(self.numSweepers):
				self.vSweepers[i].putWeights(self.vPopulation[i].vecWeights)

				self.vSweepers[i].reset()

		return True

	def render(self, surface):

		#render the stats
		s = "Generation:          " + itos(self.generations)
		#TextOut(surface, 5, 0, s.c_str(), s.size())
		print(s)

		#do not render if running at accelerated speed
		if not self.fastRender:
			#keep a record of the old pen
			#self.oldPen = SelectObject(surface, self.greenPen)
			self.oldPen = self.pen
			self.pen = "green"

			#render the mines
			for i in range(self.numMines):
				#grab the vertices for the mine shape
				mineVB = self.mineVB

				self.worldTransform(mineVB, self.vMines[i])

				#draw the mines
				#MoveToEx(surface, mineVB[0].x, mineVB[0].y, None)
				oldX = mineVB[0].x
				oldY = mineVB[0].y
				for vert in range(1,len(mineVB)):
					surface.create_line(oldX,oldY,mineVB[vert].x,mineVB[vert].y,fill=self.pen)
					oldX = mineVB[vert].x
					oldY = mineVB[vert].y
					#LineTo(surface, mineVB[vert].x, mineVB[vert].y)

				#LineTo(surface, mineVB[0].x, mineVB[0].y)
				surface.create_line(oldX,oldY,mineVB[0].x,mineVB[0].y,fill=self.pen)

			#we want the fittest displayed in red
			#SelectObject(surface, self.redPen)
			self.pen = "red"

			#render the sweepers
			for i in range(self.numSweepers):
				if (i == Params.iNumElite):
					#SelectObject(surface, self.oldPen)
					self.pen = self.oldPen

				#grab the sweeper vertices
				sweeperVB = self.sweeperVB

				#transform the vertex buffer
				self.vSweepers[i].worldTransform(sweeperVB)

				#draw the sweeper left track
				#MoveToEx(surface, sweeperVB[0].x, sweeperVB[0].y, None)
				oldX = sweeperVB[0].x
				oldY = sweeperVB[0].y

				for vert in range(1,4):
					#LineTo(surface, sweeperVB[vert].x, sweeperVB[vert].y)
					surface.create_line(oldX,oldY,sweeperVB[vert].x,sweeperVB[vert].y,fill=self.pen)
					oldX = sweeperVB[vert].x
					oldY = sweeperVB[vert].y

				#LineTo(surface, (int)sweeperVB[0].x, (int)sweeperVB[0].y)
				surface.create_line(oldX,oldY,sweeperVB[0].x,sweeperVB[0].y,fill=self.pen)

				#draw the sweeper right track
				#MoveToEx(surface, sweeperVB[4].x, sweeperVB[4].y, None)
				oldX = sweeperVB[4].x
				oldY = sweeperVB[4].y

				for vert in range(5,8):
					#LineTo(surface, sweeperVB[vert].x, sweeperVB[vert].y)
					surface.create_line(oldX,oldY,sweeperVB[vert].x,sweeperVB[vert].y,fill=self.pen)
					oldX = sweeperVB[vert].x
					oldY = sweeperVB[vert].y


				#LineTo(surface, sweeperVB[4].x, sweeperVB[4].y)
				surface.create_line(oldX,oldY,sweeperVB[4].x,sweeperVB[4].y,fill=self.pen)

				#MoveToEx(surface, sweeperVB[8].x, sweeperVB[8].y, None)
				oldX = sweeperVB[8].x
				oldY = sweeperVB[8].y
				#LineTo(surface, sweeperVB[9].x, sweeperVB[9].y)
				surface.create_line(oldX,oldY,sweeperVB[9].x,sweeperVB[9].y,fill=self.pen)

				#MoveToEx(surface, sweeperVB[10].x, sweeperVB[10].y, None)
				oldX = sweeperVB[10].x
				oldY = sweeperVB[10].y

				for vert in range(11,16):
					#LineTo(surface, sweeperVB[vert].x, sweeperVB[vert].y)
					surface.create_line(oldX,oldY,sweeperVB[vert].x,sweeperVB[vert].y,fill=self.pen)
					oldX = sweeperVB[vert].x
					oldY = sweeperVB[vert].y


			#put the old pen back
			#SelectObject(surface, self.oldPen)
			self.pen = self.oldPen
		else:
			self.plotStats(surface)

	def plotStats(self, surface):
		s = "Best Fitness:       " + self.genAlg.bestFitness()
		#TextOut(surface, 5, 20, s.c_str(), s.size());
		print(s)

		s = "Average Fitness: " + self.genAlg.averageFitness()
		#TextOut(surface, 5, 40, s.c_str(), s.size())
		print(s)

		#render the graph
		HSlice = cxClient/(self.generations+1)
		VSlice = cyClient/((self.genAlg.bestFitness()+1)*2)

		#plot the graph for the best fitness
		x = 0.0

		#self.oldPen = SelectObject(surface, self.redPen)
		self.oldPen = self.pen
		self.pen = "red"

		#MoveToEx(surface, 0, cyClient, None)
		oldX = 0
		oldY = cyClient

		for i in range(len(self.vectBestFitness)):
		   #LineTo(surface, x, cyClient - VSlice*self.vecBestFitness[i])
		   surface.create_line(oldX,oldY,x,cyClient - VSlice*self.vecBestFitness[i], fill=self.pen)

		   x += HSlice

		#plot the graph for the average fitness
		x = 0.0

		#SelectObject(surface, self.bluePen)
		self.pen = "blue"

		#MoveToEx(surface, 0, cyClient, None)
		oldX = 0
		oldY = cyClient

		for i in range(len(self.vecAvFitness)):
		   #LineTo(surface, x, (cyClient - VSlice*self.vecAvFitness[i]))
		   surface.create_line(oldX,oldY,x,cyClient - VSlice*self.vecAvFitness[i], fill=self.pen)

		   x += HSlice

		#replace the old pen
		#SelectObject(surface, self.oldPen)
		self.Pen = self.oldPen
