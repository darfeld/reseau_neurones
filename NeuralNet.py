from random import random
from math import exp
from Params import Params

class SNeuron:
# the Neuron Class
    #ctor
    def __init__(self, nbInputs):
        #the weights for each input
        self.m_vecWeight = []
        self.m_NumInputs = nbInputs + 1
        for i in range(self.m_NumInputs):
            self.m_vecWeight.append(random())

class SNeuronLayer:
# a Neuron Layer
    def __init__(self, NumNeurons, NumInputsPerNeuron):
        self.m_NumNeurons = NumNeurons
        self.m_vecNeurons = []
        for i in range(NumNeurons):
            self.m_vecNeurons.append( SNeuron(NumInputsPerNeuron) )

class CNeuralNet:

    def __init__(self):
        self.m_NumInputs = Params.iNumInputs
        self.m_NumOutPuts = Params.iNumOutputs
        self.m_NumHiddenLayers = Params.iNumHidden
        self.m_NeuronsPerHiddenLayer = Params.iNeuronsPerHiddenLayer
        self.m_vecLayers = []
		
        self.createNet()

    def createNet(self):
        if self.m_NumHiddenLayers > 0:
            self.m_vecLayers.append(SNeuronLayer(self.m_NeuronsPerHiddenLayer, self.m_NumInputs))

            for i in range(self.m_NumHiddenLayers-1):
                self.m_vecLayers.append(SNeuronLayer(self.m_NeuronsPerHiddenLayer, self.m_NeuronsPerHiddenLayer))

            self.m_vecLayers.append(SNeuronLayer(self.m_NumOutPuts, self.m_NeuronsPerHiddenLayer))

        else:
            self.m_vecLayers.append(SNeuronLayer(self.m_NumOutPuts, self.m_NumImputs))


    def getWeights(self):
        weight = []
        for i in range(self.m_NumHiddenLayers+1):
            for j in range(self.m_vecLayers[i].m_NumNeurons):
                for k in range(self.m_vecLayers[i].m_vecNeurons[j].m_NumInputs):
                    weights.append(self.m_vecLayers[i].m_vecNeurons[j].m_vecWeight[k])
        return weight

    def getNumberOfWeights(self):
        cWeight = 0
        for i in range(self.m_NumHiddenLayers+1):
            for j in range(self.m_vecLayers[i].m_NumNeurons):
                for k in range(self.m_vecLayers[i].m_vecNeurons[j].m_NumInputs):
                    cWeight+=1
        return cWeight

    def putWeights(self, weights):
        cWeight = 0
        for i in range(self.m_NumHiddenLayers+1):
            for j in range(self.m_vecLayers[i].m_NumNeurons):
                for k in range(self.m_vecLayers[i].m_vecNeurons[j].m_NumInputs):
                    self.m_vecLayers[i].m_vecNeurons[j].m_vecWeight[k] = weights[cWeight]
                    cWeight += 1

    def update(self, inputs):
        outputs = []

        if len(inputs) != self.m_NumInputs:
            return outputs

        for i in range(self.m_NumHiddenLayers+1):
            if i>0:
                inputs = list(outputs)
			
            outputs.clear()

            for j in range(self.m_vecLayers[i].m_NumNeurons):
                netinput = 0
                NumInputs = self.m_vecLayers[i].m_vecNeurons[j].m_NumInputs

                cWeight = 0
                for k in range(NumInputs-1):
                    netinput += self.m_vecLayers[i].m_vecNeurons[j].m_vecWeight[k] * inputs[cWeight]
                    cWeight += 1

                netinput += self.m_vecLayers[i].m_vecNeurons[j].m_vecWeight[NumInputs-1] * Params.dBias

                outputs.append(self.sigmoid(netinput, Params.dActivationResponse))

        return outputs


    def sigmoid(self, netinput, response):
        return ( 1 / ( 1 + exp(-netinput/response)))
