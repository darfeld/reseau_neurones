# Simple matrix computation
import math

class Simple3x3Matrix:
    def IDENTITY(self): return [[1,0,0],[0,1,0],[0,0,1]]

    def __init__(self):
        self.m_mat = self.IDENTITY()

    def translate(self, x, y):
        mat = self.IDENTITY()
        mat[2][0] = x
        mat[2][1] = y

        self.matrixMultiply(mat)


    def scale(self, xScale, yScale):
        mat = self.IDENTITY()

        mat[0][0] = xScale
        mat[1][1] = yScale

        self.matrixMultiply(mat)

    def rotate(self, rot):
        mat = self.IDENTITY()

        mat[0][0] = math.cos(rot)
        mat[0][1] = math.sin(rot)
        mat[1][0] = - math.sin(rot)
        mat[1][1] = math.cos(rot)

        self.matrixMultiply(mat)

    def matrixMultiply(self,mat):
        tmp = [[0 for i in range(3)] for j in range(3)]

        tmp[0][0] = (self.m_mat[0][0]*mat[0][0]) + (self.m_mat[0][1]*mat[1][0]) + (self.m_mat[0][2]*mat[2][0])
        tmp[0][1] = (self.m_mat[0][0]*mat[0][1]) + (self.m_mat[0][1]*mat[1][1]) + (self.m_mat[0][2]*mat[2][1])
        tmp[0][2] = (self.m_mat[0][0]*mat[0][2]) + (self.m_mat[0][1]*mat[1][2]) + (self.m_mat[0][2]*mat[2][2])

        tmp[1][0] = self.m_mat[1][0]*mat[0][0] + self.m_mat[1][1]*mat[1][0] + self.m_mat[1][2]*mat[2][0]
        tmp[1][1] = self.m_mat[1][0]*mat[0][1] + self.m_mat[1][1]*mat[1][1] + self.m_mat[1][2]*mat[2][1]
        tmp[1][2] = self.m_mat[1][0]*mat[0][2] + self.m_mat[1][1]*mat[1][2] + self.m_mat[1][2]*mat[2][2]

        tmp[2][0] = self.m_mat[2][0]*mat[0][0] + self.m_mat[2][1]*mat[1][0] + self.m_mat[2][2]*mat[2][0]
        tmp[2][1] = self.m_mat[2][0]*mat[0][1] + self.m_mat[2][1]*mat[1][1] + self.m_mat[2][2]*mat[2][1]
        tmp[2][2] = self.m_mat[2][0]*mat[0][2] + self.m_mat[2][1]*mat[1][2] + self.m_mat[2][2]*mat[2][2]

        self.m_mat = tmp

    def transformSPoints(self,points):
        for pt in points:
            tempX = self.m_mat[0][0]*pt.x + self.m_mat[1][0]*pt.y + self.m_mat[2][0]
            tempY = self.m_mat[0][1]*pt.x + self.m_mat[1][1]*pt.y + self.m_mat[2][1]
            pt.x = tempX
            pt.y = tempY

    def __repr__(self):
        s = ""
        for row in self.m_mat:
            s += "| "
            for i in row:
                s += str(i) + " "

            s += "|\n"
        return s

