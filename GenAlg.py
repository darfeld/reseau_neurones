import Utils
import Params
from random import random
from random import randint

class Genome:

    def __init__(self, vecWeights = [], dFitness = 0.0):
        self.vecWeights = vecWeights
        self.dFitness = dFitness

    def __lt__(self, gen):
        return self.dFitness < gen.dFitness

class GenAlg:

    def __init__(self, popsize = 0, mutRat=0.0, crossRat=0.0, numWeights=0):
        self.iPopSize = popsize
        self.mutationRate = mutRat
        self.crossoverRate = crossRat
        self.iChromoLength = numWeights
        self.totalFitness = 0
        self.generation = 0
        self.iFittestGenome = 0
        self.bestFitness = 0
        self.worstFitness = 99999999
        self.averageFitness = 0
        self.vecPop = []

        for i in range(popsize):
            self.vecPop.append(Genome())
            for j in range(numWeights):
                self.vecPop[i].vecWeights.append(random() - random())

    def averageFitness(self):
        return self.totalFitness/self.iPopSize
		
    def getChromos(self):
        return self.vecPop
		
    def bestFitness(self):
        return self.bestFitness

    def mutate(self, vChromo):
        for gen in vChromo:
            if(random() < self.mutationRate):
                gen += ((random() - random()) * Params.dMaxPerturbation)

    def getChromoRoulette(self):
        Slice = random() * totalFitness
        TheChosenOne = Genome()
        fitnessSoFar = 0

        for genome in self.vecPop:
            fitnessSoFar += genome.dFitness

            if(fitnessSoFar >= Slice):
                TheChosenOne = genome
                break

        return TheChosenOne

    def crossOver(self, vMum = [], vDad = [], vBaby1 = [], vBaby2 = []):
        if( (random() > self.crossoverRate) or (mum == dad) ):
            vBaby1 = vMum
            vBaby2 = vDad
            return

        cp = randint(self.iChromoLength - 1)

        for i in range(cp):
            vBaby1.append(vMum[i])
            vBaby2.append(vDad[i])

        for i in range(cp,vMum.len):
            vBaby1.append(vDad[i])
            vBaby2.append(vMum[i])

    def epoch(self, vOld_pop):
        self.vecPop = vOld_pop
        self.reset()
        self.vectPop.sort()
        self.calculateBestWorstAvTot()

        vecNewPop = []
        if((Params.iNumCopiesElite * Params.iNumElite %2) == 0 ):
            self.GrabNBest(Params.iNumElite, Params.iNumCopiesElite, vecNewPop)

        while vecNemPop.len < iPopSize:
            mum = self.getChromoRoulette()
            dad = self.getChromoRoulette()

            baby1 = []
            baby2 = []

            self.crossover(mum.vecWeights, dad.vecWeights, baby1, baby2)

            self.mutate(baby1)
            self.mutate(baby2)

            vecNewPop.append(Genome(baby1,0))
            vecNewPop.append(Genome(baby2,0))

        self.vecPop = vecNewPop

        return vecPop

    def gradNBest(self, NBest, NumCopies, vPop):
        for i in reversed(range(NBest)):
            for j in range(NumCopies):
                vPop.append(self.vecPop[(self.iPopSize - 1)])

    def calculateBestWorstAvTot(self):
        self.totalFitness = 0

        highestSoFar = 0
        lowestSoFar = 99999999

        for i in range(self.iPopSize):
            if(self.vecPop[i].dFitness > highestSoFar):
                highestSoFar = self.vecPop.dFitness
                self.iFittestGenome = i
                self.bestFitness = highestSoFar

            if(self.vecPop[i].dFitness < lowestSoFar):
                lowestSoFar = self.vecPop[i].dFitness
                self.worstFitness = lowestSoFar

            self.totalFitness += vecPop[i].dFitness

        self.averageFitness = self.totalFitness / self.iPopSize

    def reset(self):
        self.totalFitness = 0
        self.bestFitness = 0
        self.worstFitness = 99999999
        self.averageFitness = 0
